var btn = document.getElementById("btn");
var resultado = document.getElementById("resultado")

var inputUno = document.getElementById("input-uno");
var inputDos = document.getElementById("input-dos");

btn.addEventListener("click", function () {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    resultado.innerHTML = suma(n1, n2);
    resultado.style.color = "black";
});

btnR.addEventListener("click", function () {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    resultado.innerHTML = restar(n1, n2);
    resultado.style.color = "black";
});

btnM.addEventListener("click", function () {
    var n1 = inputUno.value;
    var n2 = inputDos.value;
    resultado.innerHTML = multiplicar(n1, n2);
    resultado.style.color = "black";
});

btnD.addEventListener("click", function () {
    var n1 = inputUno.value;
    var n2 = inputDos.value;

    if (n2 == 0) {
        resultado.innerHTML = "Error: No se puede dividir por 0";
        resultado.style.color = "red";
    }else{
        resultado.innerHTML = dividir(n1, n2);
        resultado.style.color = "black";
    }
});

function suma(n1, n2) {
    return parseInt(n1) + parseInt(n2);
}


function restar(n1, n2){
    return parseInt(n1)-parseInt(n2);
}


function multiplicar(n1, n2){
    return parseInt(n1)*parseInt(n2);
}


function dividir(n1, n2){
    return parseInt(n1)/parseInt(n2);
}
